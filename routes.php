<?php
/**
 * აბრუნებს რაუტებს და მათზე მიბმულ კონტროლერებს
 * კონტროლერი შეიძლება იყოს ან
 * array: პირველი ელემენტი კლასის სახელი, მეროე - მეთოდის
 * callable
 */

return [
    '/' => [\Barca\Controllers\Index::class, 'index'],

    '/redirect' => [\Barca\Controllers\Index::class, 'redirect'],

    '/test' => function () {
        return new \Barca\Application\Response(view('index.php'));
    },

    '/admin' => [\Barca\Controllers\Admin\Dashboard::class, 'index'],

    '/admin/login' => [\Barca\Controllers\Admin\Login::class, 'index'],

    '/admin/login-post' => [\Barca\Controllers\Admin\Login::class, 'login'],

    '/admin/logout' => [\Barca\Controllers\Admin\Login::class, 'logout'],

    '/admin/players' => [\Barca\Controllers\Admin\Players::class, 'index'],

    '/admin/players/create' => [\Barca\Controllers\Admin\Players::class, 'create'],

    '/admin/players/store' => [\Barca\Controllers\Admin\Players::class, 'save'],

    '/admin/players/mass-delete' => [\Barca\Controllers\Admin\Players::class, 'massDelete'],

    '/admin/users' => [\Barca\Controllers\Admin\Users::class, 'index'],

    '/admin/download-users' => [\Barca\Controllers\Admin\Download::class, 'download'],

    '/register' => [\Barca\Controllers\Register::class, 'register'],

    '/verify' => [\Barca\Controllers\Register::class, 'verify'],

    '/sign-in' => [\Barca\Controllers\SignIn::class, 'signIn'],

    '/logout' => [\Barca\Controllers\Logout::class, 'logout'],

    '/my-favourites' => [\Barca\Controllers\MyFavourites::class, 'favourites'],

    '/gallery' => [Barca\Controllers\Gallery::class, 'show'],

    '/admin/players/edit' => [\Barca\Controllers\Admin\Players::class, 'edit'],

    '/admin/players/update' => [\Barca\Controllers\Admin\Players::class, 'update']
];
