<?php

namespace Barca\Controllers;

use Barca\Application\Db;
use Barca\Application\Response;

class SignIn
{
    public function signIn()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        switch ($method) {
            case 'GET':
                return $this->loadSignInView();
            case 'POST':
                return $this->logIn();
        }
    }

    protected function loadSignInView($data = null)
    {
        if (isset($_SESSION['user_id'])) {
            return Response::redirect('/');
        }

        if (isset($_SESSION['error'])) {
            $data['error'] = $_SESSION['error'];
            $_SESSION['error'] = null;
        }

        return new Response(view('signin.php', $data));
    }

    protected function logIn()
    {
        $email = $_POST['email'];
        $password = $_POST['password'];

        $user = Db::fetch('SELECT * FROM users WHERE email = :email', [':email' => $email]);

        if (!$user) {
            $_SESSION['error'] = 'Incorrect email address';
            return $this->loadSignInView();
        }

        if (!password_verify($password, $user['password'])) {
            $_SESSION['error'] = 'Incorrect password';
            return $this->loadSignInView();
        }

        if (!$user['is_verified']) {
            $_SESSION['error'] = 'Please verify your account';
            $_SESSION['email'] = $email;
            return Response::redirect('/verify');
        }

        $_SESSION['email'] = $email;
        $_SESSION['user_id'] = $user['id'];

        return Response::redirect('/');
    }
}