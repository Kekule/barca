<?php

namespace Barca\Controllers;

use Barca\Application\Response;

class Index
{
    public function index()
    {
        $data = [
            'signedIn' => $_SESSION['user_id'] ? true : false
        ];

        return new Response(view('index.php', $data));
    }

    public function redirect()
    {
        return Response::redirect('/');
    }
}
