<?php


namespace Barca\Controllers;

use Barca\Application\Response;

class Logout
{
    public function logout() {
        $_SESSION['email'] = null;
        $_SESSION['user_id'] = null;

        return Response::redirect('/sign-in');
    }
}