<?php


namespace Barca\Controllers;

use Barca\Application\Response;
use Barca\Application\Db;

class Gallery
{
    public function show()
    {
        $error = array_key_exists('error', $_SESSION) ? $_SESSION['error'] : null;
        $message = array_key_exists('message', $_SESSION) ? $_SESSION['message'] : null;
        $_SESSION['error'] = null;
        $_SESSION['message'] = null;
        return new Response(view(
            'gallery.php',
            [
                'players' => $this->getAllPlayers(),
                'error' => $error,
                'message' => $message
            ]
        ));
    }

    private function getAllPlayers()
    {
        try {
            $players = Db::fetchAll('SELECT * FROM players');
        } catch (\Exception $ex) {
            die();
        }

        return $players;
    }
}