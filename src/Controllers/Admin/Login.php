<?php

namespace Barca\Controllers\Admin;

use Barca\Application\Response;
use Barca\Models\AdminUser;

/**
 * Class Login
 * @package Barca\Controllers\Admin
 */
class Login
{
    /**
     * გამოიტანს ლოგინის ფორმას
     * @return Response
     */
    public function index()
    {
        if (AdminUser::getCurrentUser()) {
            return Response::redirect('/admin');
        }

        $errors = isset($_SESSION['errors']) ? $_SESSION['errors'] : null;
        unset($_SESSION['errors']);

        return new Response(view('admin/login/index.php', ['errors' => $errors]));
    }

    /**
     * ალოგინებს მომხმარებელს
     * @return Response
     */
    public function login()
    {
        $username = $_POST['username'];
        $password = $_POST['password'];

        try {
            $adminUser = AdminUser::loadByUsername($username);

            if (!$adminUser->getId()) {
                $_SESSION['errors'] = [
                    'Incorrect username'
                ];

                return Response::redirect('/admin/login');
            }

            if (!password_verify($password, $adminUser->getPassword())) {
                $_SESSION['errors'] = [
                    'Incorrect password'
                ];

                return Response::redirect('/admin/login');
            }
        } catch (\Exception $e) {
            $_SESSION['errors'] = [
                'We could not find user with specified username'
            ];

            return Response::redirect('/admin/login');
        }

        $_SESSION['logged_in_admin'] = $adminUser;
        return Response::redirect('/admin');
    }

    /**
     * ალოგაუთებს მომხმარებელს
     * @return Response
     */
    public function logout()
    {
        if (isset($_POST['logout'])) {
            $currentUser = AdminUser::getCurrentUser();

            if ($currentUser) {
                $currentUser->logout();

                return Response::redirect('/admin/login');
            }
        }

        return Response::redirect('/admin');
    }
}
