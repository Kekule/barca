<?php

namespace Barca\Controllers\Admin;

use Barca\Application\DataObject;
use Barca\Application\PagerHelper;
use Barca\Application\Response;
use Barca\Models\AdminUser;
use Barca\Models\Player;
use Exception;

class Players
{
    const PER_PAGE = 10;

    /**
     * აჩვენებს დამატებული მოთამაშეების გრიდს
     * @throws Exception
     */
    public function index()
    {
        if (AdminUser::getCurrentUser() === null) {
            return Response::redirect('/admin/users');
        }

        $page = isset($_GET['p']) ? $_GET['p'] : 0;

        try {
            $players = Player::paginate($page, self::PER_PAGE);
            $total = Player::count();
        } catch (Exception $exception) {
            $_SESSION['errors'] = [
                'Could not fetch players'
            ];

            $players = [];
            $total = 0;
        }

        return new Response(view('admin/players/index.php', [
            'players' => $players,
            'total' => $total,
            'pagerData' => PagerHelper::getPagerData(self::PER_PAGE, $page, $total, 3)
        ]));
    }

    /**
     * აჩვენებს ფორმას ახალი მოთამაშის დასამატებლად
     * @return Response
     */
    public function create()
    {
        if (AdminUser::getCurrentUser() === null) {
            return Response::redirect('/admin/users');
        }

        return new Response(view('admin/players/create.php'));
    }

    /**
     * ინახავს ახალ მოთამაშეს ბაზაში და სურათს public/media/players ფოლდერში
     * @return Response
     */
    public function save()
    {
        if (AdminUser::getCurrentUser() === null) {
            return Response::redirect('/admin/users');
        }

        $player = new Player();
        $player->setData((new DataObject($_POST))->getData(Player::ALL_KEYS_WITHOUT_ID));

        try {
            $player->setImage($this->uploadImages());
            $player->save();
        } catch (Exception $exception) {
            $_SESSION['errors'] = [
                'Failed to create player.'
            ];
        }

        return Response::redirect('/admin/players');
    }

    /**
     * შლის ბევრ ფეხბურთელს ერთდროულად
     * @return Response
     */
    public function massDelete()
    {
        if (AdminUser::getCurrentUser() === null) {
            return Response::redirect('/admin/users');
        }

        $items = isset($_POST['items']) ? $_POST['items'] : null;

        if ($items) {
            $itemsJson = json_decode($items);

            if (!json_last_error()) {
                Player::massDelete($itemsJson);
            }
        }

        return Response::redirect('/admin/players');
    }

    /**
     * აჩვენებს მოთამაშის ედიტის ფორმას
     * @return Response
     * @throws Exception
     */
    public function edit()
    {
        if (AdminUser::getCurrentUser() === null) {
            return Response::redirect('/admin/users');
        }

        if (!isset($_GET['id'])) {
            return Response::redirect('/admin/players');
        }

        $player = Player::loadById($_GET['id']);

        if (!$player) {
            return Response::redirect('/admin/players');
        }

        return new Response(view('admin/players/edit.php', [
            'player' => $player
        ]));
    }

    /**
     * ააფდეითებს არსებულ მოთამაშეს (მის სურათსაც)
     * @return Response
     */
    public function update()
    {
        if (AdminUser::getCurrentUser() === null) {
            return Response::redirect('/admin/users');
        }

        $postData = $_POST;
        if ($this->imageWasUploaded()) {
            try {
                $postData['image'] = $this->uploadImages();
            } catch (Exception $exception) {
                $_SESSION['errors'] = [$exception->getMessage()];

                return Response::redirect('/admin/players');
            }
        }

        try {
            $player = Player::loadById($postData['id']);
            $oldImage = $player->getImage();
            $player->mergeData($postData)
                ->save();

            if ($oldImage !== $player->getImage()) {
                unlink(public_path('media/players/' . $oldImage));
            }

            $_SESSION['messages'] = [
                'Successfully updated player'
            ];
        } catch (Exception $exception) {
            $_SESSION['errors'] = [
                'Could not save player'
            ];
        }

        return Response::redirect('/admin/players');
    }

    /**
     * ტვირთავს სურათს, აბრუნებს სურათის სახელს
     * @return string
     * @throws Exception
     */
    private function uploadImages()
    {
        if (AdminUser::getCurrentUser() === null) {
            return Response::redirect('/admin/users');
        }

        $target_dir = public_path('media/players/');
        $target_file = $target_dir . basename($_FILES['image']['name']);
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

        $check = getimagesize($_FILES["image"]["tmp_name"]);
        if (!$check) {
            throw new Exception('File is not an image');
        }

        if (!in_array($imageFileType, ['jpg', 'png', 'jpeg', 'gif'])) {
            throw new Exception('File must be one of following types: jpeg, jpg, png, gif');
        }

        $fileName = round(microtime(true) * 1000) . '.' . $imageFileType;
        $target_file = $target_dir . $fileName;

        if (!move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
            throw new Exception('An error has occurred while uploading the file');
        }

        return $fileName;
    }

    /**
     * ამოწმებს ატვირთული სურათის ვალიდურობას
     * @return bool
     */
    private function imageWasUploaded()
    {
        if (AdminUser::getCurrentUser() === null) {
            return Response::redirect('/admin/users');
        }

        if (!isset($_FILES['image'])) {
            return false;
        }

        $check = getimagesize($_FILES["image"]["tmp_name"]);

        return (bool)$check;
    }
}
