<?php


namespace Barca\Controllers\Admin;

use Barca\Application\Db;
use Barca\Application\Response;

class Download
{
    /**
     * @return Response
     * იწერს მომხმარებლებს ფაილში
     */
    public function download()
    {
        if (!isset($_SESSION['logged_in_admin'])) {
            return Response::redirect('/admin/login');
        }

        try {
            $users = Db::fetchAll('SELECT * FROM users');
        } catch (\Exception $ex) {
            die($ex->getMessage());
        }

        $this->array_to_csv_download($users, "users.csv");
        die;
    }

    protected function array_to_csv_download($array, $filename = 'export.csv', $delimiter=";") {
        $f = fopen('php://memory', 'w');

        foreach ($array as $line) {
            fputcsv($f, $line, $delimiter);
        }

        fseek($f, 0);
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="'.$filename.'";');
        fpassthru($f);
    }
}