<?php


namespace Barca\Controllers\Admin;

use Barca\Application\Db;
use Barca\Application\Response;

class Users
{
    /**
     * აბრუნებს მომხმარებლების ლისტს
     * @return Response
     */
    public function index() {
        if (!isset($_SESSION['logged_in_admin'])) {
            return Response::redirect('/admin/login');
        }

        return new Response(view('admin/users/index.php', ['users' => $this->getAllUser()]));
    }

    private function getAllUser() {
        try {
            $users = Db::fetchAll('SELECT * FROM users');
        } catch (\Exception $ex) {
            die($ex->getMessage());
        }

        return $users;
    }
}