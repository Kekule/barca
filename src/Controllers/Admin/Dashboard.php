<?php

namespace Barca\Controllers\Admin;

use Barca\Application\Response;

/**
 * Class Dashboard
 * @package Barca\Controllers\Admin
 */
class Dashboard
{
    /**
     * @return Response
     * ადმინ პანელის მთავარი გვერდი (ცარიელია)
     */
    public function index()
    {
        if (!isset($_SESSION['logged_in_admin'])) {
            return Response::redirect('/admin/login');
        }

        return new Response(view('admin/dashboard.php', [
            'admin' => $_SESSION['logged_in_admin']
        ]));
    }
}
