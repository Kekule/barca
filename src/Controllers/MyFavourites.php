<?php


namespace Barca\Controllers;

use Barca\Application\Db;
use Barca\Application\Response;

class MyFavourites
{
    /**
     * @return Response
     */
    public function favourites()
    {
        if (!$_SESSION['user_id']) return Response::redirect('/');

        $method = $_SERVER['REQUEST_METHOD'];
        switch ($method) {
            case 'GET':
                return $this->loadFavouritesView();
            case 'POST':
                return $this->addToFavourites();
        }
    }

    /**
     * @return Response
     */
    private function loadFavouritesView()
    {
        $data = [
            'kumiri' => $this->getUsersKumiriPlayer(),
            'favourites' => $this->getUsersFavouritePlayers()
        ];

        if ($_SESSION['message']) $data['message'] = $_SESSION['message'];
        if ($_SESSION['error']) $data['error'] = $_SESSION['error'];

        $_SESSION['message'] = null;
        $_SESSION['error'] = null;

        return new Response(view('favourites.php', $data));
    }

    /**
     * @return Response
     */
    private function addToFavourites()
    {
        if (array_key_exists('type', $_GET)) {
            switch ($_GET['type']) {
                case 'favourite':
                    return $this->addPlayerToFavourites();
                    break;
                case 'kumiri':
                    return $this->addPlayerToKumiri();
                    break;
                case 'delete-favourite':
                    return $this->deleteFavourite();
                    break;
                case 'delete-kumiri':
                    return $this->deleteKumiri();
                    break;
                default:
                    $_SESSION['error'] = 'Something wen`t wrong';
                    return Response::redirect('/gallery');
            }
        } else {
            $_SESSION['error'] = 'Something wen`t wrong';
            return Response::redirect('/gallery');
        }
    }

    /**
     * @return Response
     * @throws \Exception
     */
    private function addPlayerToFavourites()
    {
        $playerId = $_POST['player-id'];
        $userId = $_SESSION['user_id'];

        $exists = Db::fetch(
            'SELECT * FROM favourites WHERE player_id = :player_id AND user_id = :user_id',
            [
                ':player_id' => $playerId,
                ':user_id' => $userId
            ]
        );

        if ($exists) {
            $_SESSION['error'] = 'ეს მოთამაშე უკვე დამატებულია თქვენი ფავორიტების სიაში';
            return Response::redirect('/gallery');
        }

        try {
            Db::execute(
                'INSERT INTO favourites (user_id, player_id) VALUES (:user_id, :player_id)',
                [
                    ':user_id' => $userId,
                    ':player_id' => $playerId,
                ]
            );
        } catch (\Exception $ex) {
            die($ex->getMessage());
        }

        $_SESSION['message'] = 'ფავორიტი წარმატებით აირჩა';
        return Response::redirect('/gallery');
    }

    /**
     * @return Response
     * @throws \Exception
     */
    private function addPlayerToKumiri()
    {
        $playerId = $_POST['player-id'];
        $userId = $_SESSION['user_id'];

        $alreadyHasKumiri = Db::fetch(
            'SELECT * FROM users WHERE id = :user_id AND favourite_player IS NOT NULL',
            [
                ':user_id' => $userId
            ]
        );

        if ($alreadyHasKumiri) {
            $_SESSION['error'] = 'თქვენ უკვე გყავთ ერთი კუმირი და იკმარეთ';
            return Response::redirect('/gallery');
        }

        try {
            Db::execute(
                'UPDATE users SET favourite_player = :kumiri WHERE id = :user_id',
                [
                    ':kumiri' => $playerId,
                    ':user_id' => $userId
                ]
            );
        } catch (\Exception $ex) {
            die($ex->getMessage());
        }

        $_SESSION['message'] = 'გილოცავთ!!! თქვენ აირჩიეთ კუმირი';
        return Response::redirect('/gallery');
    }

    public function deleteFavourite()
    {
        $userId = $_SESSION['user_id'];
        $playerId = $_POST['player-id'];

        try {
            Db::execute(
                'DELETE FROM favourites WHERE player_id = :player_id AND user_id = :user_id',
                [
                    ':user_id' => $userId,
                    ':player_id' => $playerId
                ]
            );
        } catch (\Exception $ex) {
            die($ex->getMessage());
        }

        $_SESSION['message'] = 'თქვენ წაშალეთ ფავორიტი მოთამაშე';
        return Response::redirect('/my-favourites');
    }

    public function deleteKumiri()
    {
        $userId = $_SESSION['user_id'];

        try {
            Db::execute(
                'update users set favourite_player = null where id = :user_id',
                [
                    ':user_id' => $userId
                ]
            );
        } catch (\Exception $ex) {
            die($ex->getMessage());
        }

        $_SESSION['message'] = 'თქვენ წაშალეთ კუმირი. კუმირის წაშლა არ გეპატიებათ!!!';
        return Response::redirect('/my-favourites');
    }

    /**
     * @return array
     */
    private function getUsersFavouritePlayers()
    {
        $userId = $_SESSION['user_id'];

        try {
            $players = Db::fetchAll(
                'select * from players as p join favourites as f on p.id = f.player_id where f.user_id = :user_id',
                [
                    ':user_id' => $userId
                ]
            );
        } catch (\Exception $ex) {
            die($ex->getMessage());
        }

        return $players;
    }

    /**
     * @return mixed
     */
    private function getUsersKumiriPlayer()
    {
        $userId = $_SESSION['user_id'];

        try {
            $kumiri = Db::fetch(
                'select * from players as p join users as u on p.id = u.favourite_player where u.id = :user_id',
                [
                    ':user_id' => $userId
                ]
            );
        } catch (\Exception $ex) {
            die($ex->getMessage());
        }

        return $kumiri;
    }
}