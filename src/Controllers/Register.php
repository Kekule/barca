<?php


namespace Barca\Controllers;

use Barca\Application\Db;
use PHPMailer\PHPMailer\PHPMailer;
use Barca\Application\Response;

class Register
{
    public function register()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        switch ($method) {
            case 'GET':
                return $this->loadRegisterView();
            case 'POST':
                return $this->registerUser();
        }
    }

    public function verify()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        switch ($method) {
            case 'GET':
                return $this->loadVerificationView();
            case 'POST':
                return $this->verifyUser();
        }
    }

    protected function loadRegisterView($data = null)
    {
        if (isset($_SESSION['user_id'])) {
            return Response::redirect('/');
        }

        if ($_SESSION['error']) {
            $data['error'] = $_SESSION['error'];
            $_SESSION['error'] = null;
        }

        return new Response(view('register.php', $data));
    }

    protected function registerUser()
    {
        $username = $_POST['username'];
        $email = $_POST['email'];
        $password = password_hash($_POST['password'], PASSWORD_DEFAULT);
        $code = mt_rand(100000, 999999);

        $user = Db::fetch('SELECT * FROM users WHERE email = :email', [':email' => $email]);

        if ($user) {
            $_SESSION['error'] = 'User with given email already exists';
            return $this->loadRegisterView();
        }

        try {
            Db::execute(
                'INSERT INTO users (username, email, password, verification_code) VALUES (:username, :email, :password, :code)',
                [
                    ':username' => $username,
                    ':email' => $email,
                    ':password' => $password,
                    ':code' => $code
                ]
            );
        } catch (\Exception $ex) {
            die($ex->getMessage());
        }

        try {
            $this->sendMail($email, $username, $code);
        } catch (\Exception $ex) {
            die($ex->getMessage());
        }

        $_SESSION['email'] = $email;

        return Response::redirect('/verify');
    }

    protected function loadVerificationView($data = null)
    {
        if (!isset($_SESSION['email'])) return Response::redirect('/register');

        if ($_SESSION['error']) {
            $data['error'] = $_SESSION['error'];
            $_SESSION['error'] = null;
        }

        return new Response(view('verify.php', $data));
    }

    protected function verifyUser()
    {
        $code = $_POST['code'];
        $email = $_SESSION['email'];

        $user = Db::fetch('SELECT * FROM users WHERE email = :email', [':email' => $email]);

        if (!$user) {
            $_SESSION['error'] = 'something went wrong';
            return $this->loadVerificationView();
        }

        if ($code !== $user['verification_code']) {
            $_SESSION['error'] = 'incorrect verification code';
            return $this->loadVerificationView();
        }

        try {
            Db::execute(
                'UPDATE users SET is_verified = :is_verified WHERE email = :email',
                [
                    ':is_verified' => true,
                    ':email' => $email
                ]
            );
        } catch (\Exception $ex) {
            die($ex->getMessage());
        }

        $_SESSION['user_id'] = $user['id'];

        return Response::redirect('/');
    }

    private function sendMail($email, $username, $code)
    {
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->Host = config()['smpt']['host'];
        $mail->SMTPAuth = true;
        $mail->Username = 'df8cb6612c47f8';
        $mail->Password = '8644e326e278bf';
        $mail->SMTPSecure = 'tls';
        $mail->Port = 2525;

        $mail->setFrom('anzor.kharazishvili@gmail.com', 'administrator');
        $mail->addAddress($email, $username);
        $mail->Subject = 'Verification email';
        $mail->isHTML(TRUE);
        $mail->Body = '<html>Your verification code is <b>' . $code . '</b></html>';

        if (!$mail->send()) {
            die('mail could not be sent ' . $mail->ErrorInfo);
        }
    }
}