<?php

namespace Barca\Models;

use Barca\Application\DataObject;
use Barca\Application\Db;
use Barca\Application\Response;
use DateTime;
use Exception;

/**
 * მოთამაშის მოდელი
 * Class Player
 * @package Barca\Models
 * @method int getId()
 * @method $this setId(int $id)
 * @method string getFirstname()
 * @method $this setFirstname(string $firstname)
 * @method string getLastname()
 * @method $this setLastname(string $lastname)
 * @method string|null getNickname()
 * @method $this setNickname(string $nickname)
 * @method DateTime getDateOfBirth()
 * @method $this setDateOfBirth(DateTime $dateOfBirth)
 * @method int getHeight()
 * @method $this setHeight(int $height)
 * @method int getWeight()
 * @method $this setWeight(int $weight)
 * @method string getSmallBio()
 * @method $this setSmallBio(string $smallBio)
 * @method string getImage()
 * @method $this setImage(string $image)
 * @method int getJerseyNumber()
 * @method $this setJerseyNumber(string $jerseyNumber)
 */
class Player extends DataObject
{
    const ALL_KEYS = [
        'id',
        'firstname',
        'lastname',
        'nickname',
        'date_of_birth',
        'height',
        'weight',
        'small_bio',
        'image',
        'jersey_number'
    ];

    const ALL_KEYS_WITHOUT_ID = [
        'firstname',
        'lastname',
        'nickname',
        'date_of_birth',
        'height',
        'weight',
        'small_bio',
        'image',
        'jersey_number'
    ];

    /**
     * Player constructor.
     * @param array $data
     */
    public function __construct($data = [])
    {
        parent::__construct($data);
    }

    /**
     * ინახავს/ააფდეითებს მომხმარებელს
     * @return int
     * @throws Exception
     */
    public function save()
    {
        if ($this->getId() === null) {
            return Db::execute("INSERT INTO players (
                     firstname,
                     lastname, 
                     nickname, 
                     date_of_birth, 
                     height, 
                     weight, 
                     small_bio, 
                     image, 
                     jersey_number
                 )
                 VALUES (
                     :firstname,
                     :lastname, 
                     :nickname, 
                     :date_of_birth, 
                     :height, 
                     :weight, 
                     :small_bio, 
                     :image, 
                     :jersey_number
                 )",
            $this->getSqlParamsData(self::ALL_KEYS_WITHOUT_ID));
        }

        return Db::execute("UPDATE players SET
                firstname = :firstname,
                lastname = :lastname, 
                nickname = :nickname, 
                date_of_birth = :date_of_birth, 
                height = :height, 
                weight = :weight, 
                small_bio = :small_bio, 
                image = :image, 
                jersey_number = :jersey_number
            WHERE id = :id",
            $this->getSqlParamsData(self::ALL_KEYS)
        );
    }

    /**
     * @param $id
     * @return Player
     * @throws Exception
     */
    public static function loadById($id)
    {
        $playerData = Db::fetch("SELECT * FROM players WHERE id = ?", [$id]);

        return new Player(
            (new DataObject($playerData))->getData(self::ALL_KEYS)
        );
    }

    /**
     * აბრუნებს მოთამაშეებს pagination-ისთვის
     * @param int $perPage
     * @param int $page
     * @return Player[]
     * @throws Exception
     */
    public static function paginate($page, $perPage)
    {
        $low = $page * $perPage;
        $max = $low + $perPage;

        $playersData = Db::fetchAll("SELECT * FROM players LIMIT $low, $max");

        return array_map(function ($playerData) {
            return new Player(
                (new DataObject($playerData))->getData(self::ALL_KEYS)
            );
        }, $playersData);
    }

    /**
     * აბრუნებს მოთამაშეების ჯამურ რაოდენობას
     * @throws Exception
     */
    public static function count()
    {
        Db::execute('SELECT COUNT(*) AS count FROM players')['count'];
    }

    /**
     * შლის ბევრ მოთამაშეს ერთი ხელის მოსმით
     * @param $ids
     * @return Response
     */
    public static function massDelete($ids)
    {
        $inIds = implode(', ', array_map(function () { return '?'; }, $ids));

        try {
            $affectedRows = Db::execute("DELETE FROM players WHERE id IN ($inIds)", $ids);
            $_SESSION['messages'] = [
                "Deleted $affectedRows items"
            ];
        } catch (Exception $e) {
            $_SESSION['messages'] = [
                'Could not delete items'
            ];
        }

        return Response::redirect('/admin/players');
    }
}
