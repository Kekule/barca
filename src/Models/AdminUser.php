<?php

namespace Barca\Models;

use Barca\Application\DataObject;
use Barca\Application\Db;
use Exception;

/**
 * მოდელი ადმინის იუზერებისთვის
 * Class AdminUser
 * @package Barca\Models
 * @method string getPassword()
 * @method string getUsername()
 * @method $this setUsername(string $username)
 * @method string getFirstname()
 * @method $this setFirstname(string $firstname)
 * @method string getLastname()
 * @method $this setLastname(string $lastname)
 * @method int getId()
 * @method $this setId(int $id)
 * @method string getEmail()
 * @method $this setEmail(string $email)
 */
class AdminUser extends DataObject
{
    const ALL_KEYS = ['id', 'password', 'username', 'firstname', 'lastname', 'email'];

    const ALL_KEYS_WITHOUT_ID = ['password', 'username', 'firstname', 'lastname', 'email'];

    /**
     * AdminUser constructor.
     * @param array $data
     */
    public function __construct($data = [])
    {
        parent::__construct($data);
    }

    /**
     * სეტავს პაროლს
     * თუ $hash არის true მაშინ ჯერ დაჰეშავს, თუ არა პირადპირ დასეტავს
     * @param $password
     * @param bool $hash
     * @return AdminUser
     */
    public function setPassword($password, $hash = true)
    {
        $this->data['password'] = $hash ? password_hash($password, PASSWORD_DEFAULT) : $password;

        return $this;
    }

    /**
     * ინახავს/ააფდეითებს მომხმარებელს
     * @throws Exception
     */
    public function save()
    {
        /**
         * თუ აიდი არსებობს, ააფდეითებს მომხმარებელს
         */
        if ($this->getId() === null) {
            return Db::execute(
                "INSERT INTO admin_users
                (username, firstname, lastname, password, email)
                VALUES(:username, :firstname, :lastname, :password, :email)",
                $this->getSqlParamsData(self::ALL_KEYS_WITHOUT_ID)
            );
        }

        return Db::execute(
            "UPDATE admin_users SET 
                       username = :username, 
                       firstname = :firstname, 
                       lastname = :lastname,
                       password = :password,
                       email = :email
                    WHERE id = :id",
            $this->getSqlParamsData(self::ALL_KEYS)
        );
    }

    /**
     * ტვირთავს მომხმარებელს აიდის მიხედვით
     * @param $id
     * @return AdminUser
     * @throws Exception
     */
    public static function loadById($id)
    {
        $adminUserData = Db::fetch('SELECT * FROM admin_users WHERE id = ?', [$id]);

        return new AdminUser(
            (new DataObject($adminUserData))->getData(self::ALL_KEYS)
        );
    }

    /**
     * ტვირთავს მომხმარებელს username-ის მიხედვით
     * @param $username
     * @return AdminUser
     * @throws Exception
     */
    public static function loadByUsername($username)
    {
        $adminUserData = Db::fetch('SELECT * FROM admin_users WHERE username = ?', [$username]);

        return new AdminUser(
            (new DataObject($adminUserData))->getData(self::ALL_KEYS)
        );
    }

    /**
     * ალოგინებს მომხმარებელს
     * @return void
     */
    public function login()
    {
        $_SESSION['logged_in_admin'] = $this;
    }

    /**
     * ალოგაუთებ მომხმარებელს
     * @return void
     */
    public function logout()
    {
        unset($_SESSION['logged_in_admin']);
    }

    /**
     * აბრუნებს მიმდინარე მომხმარებელს
     * @return AdminUser|null
     */
    public static function getCurrentUser()
    {
        return isset($_SESSION['logged_in_admin']) ? $_SESSION['logged_in_admin'] : null;
    }
}
