<?php

namespace Barca\Application;

class PagerHelper
{
    /**
     * @param int $perPage
     * @param int $currentPage
     * @param int $total
     * @param int $maxAllowedLinks
     * @return array[]
     */
    public static function getPagerData($perPage, $currentPage, $total, $maxAllowedLinks) {
        $maxPages = min(ceil($total / $perPage), $currentPage + $maxAllowedLinks);
        $minPages = max(1, $currentPage - $maxAllowedLinks);

        $prevPages = [];
        $nextPages = [];

        for ($i = $currentPage - 1, $j = $currentPage; $i > 0 && $j > $minPages; $i --, $j++) {
            $prevPages[] = $i;
        }

        for ($i = $currentPage + 1, $j = $currentPage; $i < $maxPages, $j < $maxPages; $i++, $j++) {
            $nextPages[] = $i;
        }

        return [
            'prevPages' => $prevPages,
            'nextPages' => $nextPages
        ];
    }
}
