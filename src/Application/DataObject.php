<?php

namespace Barca\Application;

/**
 * Class DataObject
 * @package Barca\Application
 * უბრალოდ დამხმარე კლასი რომელიც მასივის ელემენტების წვდომას აადვილებს magic method-ებით
 * ბაზიდან მოსული ინფორმაციის ამოსაღებად მოსახერხებელია
 * $data ველში ინახება მასივად key - value სადაც, თუ key წერია snake_case-ში მის გეტერ და სეტერ მეთოდს გამოიძახებთ camlaCase-ით
 * მაგ:
 * თუ $data = ['first_name' => 'მაგდა']
 * $this->getFirstName() დააბრუნებს 'მაგდა'-ს
 * $this->>setFirstName('მირანდა') $data-ში 'first_name'-ს მიანიჭებს მნიშვნელობას 'მირანდა'
 */
class DataObject
{
    /**
     * @var array
     */
    protected $data = [];

    /**
     * DataObject constructor.
     * @param array $data
     */
    public function __construct($data = [])
    {
        $this->data = $data;
    }

    /**
     * აბრუნებს $data მასივს ან იმ ელემენტებს მასივიდან რასაც გადაეცემა $keys პარამეტრში
     * @param null|array $keys
     * @return array
     */
    public function getData($keys = null) {
        if ($keys === null) {
            return $this->data;
        }

        $result = [];

        foreach ($this->data as $key => $value) {
            if (in_array($key, $keys, true)) {
                $result[$key] = $value;
            }
        }

        return $result;
    }

    /**
     * იგივე რაც getData() მხოლოდ key-ებს წინ უმატებს ':'
     * @param null|array $keys
     * @return array
     */
    public function getSqlParamsData($keys = null) {
        $data = $this->getData($keys);

        $result = [];

        foreach ($data as $key => $value) {
            if (in_array($key, $keys, true)) {
                $result[":$key"] = $value;
            }
        }

        return $result;
    }

    /**
     * @param array $newData
     * @return $this
     */
    public function setData($newData)
    {
        $this->data = $newData;

        return $this;
    }

    /**
     * @param $data
     * @return $this
     */
    public function mergeData($data)
    {
        $this->data = array_merge($this->data, $data);

        return $this;
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        $methodType = substr($name, 0, 3);
        $camelCasedKey = substr($name, 3);
        $key = $key = ltrim(strtolower(preg_replace('/[A-Z]([A-Z](?![a-z]))*/', '_$0', $camelCasedKey)), '_');

        if ($methodType === 'set') {
            $this->data[$key] = $arguments[0];

            return $this;
        }

        if ($methodType === 'get') {
            if (isset($this->data[$key])) {
                return $this->data[$key];
            } else if (isset($arguments[0])) {
                return $arguments[0];
            }
        }

        return null;
    }
}
