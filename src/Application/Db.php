<?php

namespace Barca\Application;

use PDO;
use Exception;
use PDOException;

/**
 * Class Db
 * @package Barca\Application
 * კლასი Sql ქუერიების მარტივად გასაშვებად
 */
class Db
{
    /**
     * @var PDO
     */
    protected static $pdo;

    /**
     * აბრუნებს PDO კლასის ინსტანსს (იქმენბა ერთხელ)
     * @return PDO
     */
    private static function getPdo()
    {
        if (self::$pdo instanceof PDO) {
            return self::$pdo;
        }

        $type = config()['db']['type'];
        $host = config()['db']['host'];
        $dbName = config()['db']['dbName'];
        $user = config()['db']['user'];
        $pass = config()['db']['pass'];

        try {
            self::$pdo = new PDO("$type:host=$host;dbname=$dbName", $user, $pass);
        } catch (PDOException $exception) {
            die('Connection error');
        }

        return self::$pdo;
    }

    /**
     * აბრუნებს მასივად ყველაფერს რაც იქნება დასელექთებული
     * @param string $query
     * @param null|array $params
     * @return array
     * @throws Exception
     */
    public static function fetchAll($query, $params = null)
    {
        $pdo = self::getPdo();
        $statement = $pdo->prepare($query);

        if (!$statement->execute($params)) {
            throw new Exception("Could not run query '$query'");
        }

        return $statement->fetchAll();
    }

    /**
     * აბრუნებს პირველ დასელექტებულ row-ს
     * @param string $query
     * @param array|null $params
     * @return mixed
     * @throws \Exception
     */
    public static function fetch($query, $params = null)
    {
        $pdo = self::getPdo();
        $statement = $pdo->prepare($query);

        if (!$statement->execute($params)) {
            throw new \Exception("Could not run query '$query'");
        }

        return $statement->fetch();
    }

    /**
     * უშვებს ქუერის
     * @param string $query
     * @param null $params
     * @return int
     * @throws Exception
     */
    public static function execute($query, $params = null)
    {
        $pdo = self::getPdo();
        $statement = $pdo->prepare($query);

        if (!$statement->execute($params)) {
            throw new Exception("Could not run query '$query'");
        }

        return $statement->rowCount();
    }
}
