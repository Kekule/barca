<?php

namespace Barca\Application;

/**
 * Class Application
 * @package Barca\Application
 * მთავარი აპლიკაცია
 * ვალდებულია ყველა რექვესტზე გარდა /media/* და /assets/* რაუტებისა
 */
class Application
{
    public function run()
    {
        /**
         * აიმპორტებს წინასწარ განსაზღვრულ რაუტებს
         */
        $routes = require dirname(dirname(__DIR__)) . '/routes.php';
        $router = new Router($routes);

        /**
         * მოძებნის რაუტს რომელიც შეესაბამება მიმდინარე რექვესტს
         */
        $matchedRoute = $router->matchRoute();

        /**
         * თუ რაუტი არ არის განსაზღვრული routes.php ფაილში აბრუნებს 404ს
         */
        if ($matchedRoute === null) {
            (new Response('NO ROUTE', 404))->render();
            return;
        }

        /**
         * გამოიძახებს რაუტის კონტროლერს და დააბრუნებს რექვესტს
         */
        $this->processRoute($matchedRoute)->render();
    }

    /**
     * გამოიძახებს რაუტის კონტროლერს და მის შესაბამის მეთოდს
     * აბრუნებს კონტროლერიდან დაბრუნებულ Response კლასს
     * @param $matchedRoute
     * @return Response
     */
    protected function processRoute($matchedRoute)
    {
        /**
         * routes.php ფაილში კონტროლერი შეიძლება იყოს ორი ტიპის
         * array: პირველი ელემენტი კლასის სახელი, მეორე მეთოდის სახელი
         * callable
         */
        if (is_array($matchedRoute)) {
            $class = $matchedRoute[0];
            $method = $matchedRoute[1];

            $controller = new $class();
            return $controller->{$method}();
        }

        return $matchedRoute();
    }
}
