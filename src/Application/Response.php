<?php

namespace Barca\Application;

/**
 * რესპონს კლასი. აბრუნებენ კონტროლერები
 * Class Response
 * @package Barca\Application
 */
class Response
{
    const CODE_OK = 200;
    const CODE_REDIRECT = 301;

    /**
     * აბრუნებს რესპონსს, რომელიიც იწვევს $url-ზე გადარედირექტებას
     * @param string $url
     * @return Response
     */
    public static function redirect($url)
    {
        return (new Response())
            ->setResponseCode(self::CODE_REDIRECT)
            ->addHeader("Location: $url");
    }

    /**
     * @var int response code
     */
    protected $responseCode;

    /**
     * @var string body og the response
     */
    protected $responseBody;

    /**
     * @var string[] list of headers
     */
    protected $headers;

    /**
     * Response constructor.
     * @param string $responseBody
     * @param int $responseCode
     * @param array $headers
     */
    public function __construct($responseBody = '', $responseCode = 200, $headers = [])
    {
        $this->responseBody = $responseBody;
        $this->responseCode = $responseCode;
        $this->headers = $headers;
    }

    /**
     * @param string $body
     * @return $this
     */
    public function setBody($body)
    {
        $this->responseBody = $body;

        return $this;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->responseBody;
    }

    /**
     * @param string $header
     * @return Response
     */
    public function addHeader($header)
    {
        $this->headers[] = $header;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @return int
     */
    public function getResponseCode()
    {
        return $this->responseCode;
    }

    /**
     * @param $responseCode
     * @return $this
     */
    public function setResponseCode($responseCode)
    {
        $this->responseCode = $responseCode;

        return $this;
    }

    /**
     * არენდერებს რესპონსს სეტავს ჰედერებს, რესპონსის სტატუსს, და კონტენტს (causes side effects)
     * @return void
     */
    public function render()
    {
        $this->renderHeaders();
        $this->renderResponseCode();
        $this->renderBody();
    }

    /**
     * @return void
     */
    protected function renderHeaders()
    {
        foreach ($this->getHeaders() as $header) {
            header($header);
        }
    }

    /**
     * @return void
     */
    protected function renderResponseCode()
    {
        http_response_code($this->getResponseCode());
    }

    /**
     * @return void
     */
    protected function renderBody()
    {
        echo $this->getBody();
    }
}
