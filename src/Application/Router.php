<?php

namespace Barca\Application;

/**
 * Class Router
 * @package Barca\Application
 * არჩევს რომელი რაუტი უნდა გაეშვას მიმდინარე რექვესტის დასამუშავებლად
 */
class Router
{
    /**
     * @var array
     */
    protected $routes = [];

    /**
     * Router constructor.
     * @param array $routes
     */
    public function __construct($routes)
    {
        $this->routes = $routes;
    }

    /**
     * @return array|callable|null
     * შეარჩევს რაუტს და დააბრუნებს მის ჰენდლერს
     * @see Application::processRoute
     */
    public function matchRoute()
    {
        $pathname = '/' . Request::getPathname();

        $matched = null;

        foreach ($this->routes as $rule => $route) {
            if ($rule === $pathname) {
                $matched = $route;
            }
        }

        return $matched;
    }
}
