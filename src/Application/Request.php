<?php

namespace Barca\Application;

/**
 * Class Request
 * @package Barca\Application
 * რექვესტიდან სხვადასხვა დატის ამოსაღებად
 */
class Request
{
    /**
     * აბრუნებს pathname-ს url-დან (similar to location.pathname in Javascript)
     * @return string
     */
    public static function getPathname()
    {
        $url = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $pathname = parse_url($url, PHP_URL_PATH);

        return trim($pathname, '/');
    }
}
