<link rel="stylesheet" type="text/css" href="/assets/css/index-style.css">

<header>
    <nav>
        <ul>
            <li><a href="/">Home</a></li>
            <li><a href="/gallery">Gallery</a></li>
            <?php if (!(array_key_exists('full', $params) && !$params['full'])) : ?>
                <?php if ($_SESSION['user_id']) : ?>
                    <li><a href="/my-favourites">My Favourites</a></li>
                    <li style="float: right;"><a class="active" href="/logout">Log Out</a></li>
                <?php else : ?>
                    <li style="float: right;"><a class="active" href="/sign-in">Log IN</a></li>
                <?php endif; ?>
            <?php endif; ?>
        </ul>
    </nav>
</header>