<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/assets/css/index-style.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/button.css">
    <meta name="viewport" content="width=device-witdth, initial-scale=1">
    <title>Gallery</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script>
        function myFunction(imgs) {
            var expandImg = document.getElementById("expandedImg");
            var imgText = document.getElementById("imgtext");
            expandImg.src = imgs.src;
            imgText.innerHTML = imgs.alt;
            expandImg.parentElement.style.display = "block";
        }
    </script>
</head>
<style>
    .btn-favourite {
        box-shadow: inset 0px 1px 0px 0px #fff6af;
        background: linear-gradient(to bottom, #ffec64 5%, #ffab23 100%);
        background-color: #ffec64;
        border-radius: 6px;
        border: 1px solid #ffaa22;
        display: inline-block;
        cursor: pointer;
        color: #333333;
        font-family: Arial;
        font-size: 15px;
        font-weight: bold;
        padding: 6px 24px;
        text-decoration: none;
        text-shadow: 0px 1px 0px #ffee66;
    }

    .btn-favourite:hover {
        background: linear-gradient(to bottom, #ffab23 5%, #ffec64 100%);
        background-color: #ffab23;
    }

    .btn-favourite:active {
        position: relative;
        top: 1px;
    }

    .btn-kumiri {
        box-shadow: inset 0px 1px 0px 0px #f5978e;
        background: linear-gradient(to bottom, #f24537 5%, #c62d1f 100%);
        background-color: #f24537;
        border-radius: 6px;
        border: 1px solid #d02718;
        display: inline-block;
        cursor: pointer;
        color: #ffffff;
        font-family: Arial;
        font-size: 15px;
        font-weight: bold;
        padding: 6px 24px;
        text-decoration: none;
        text-shadow: 0px 1px 0px #810e05;
    }

    .btn-kumiri:hover {
        background: linear-gradient(to bottom, #c62d1f 5%, #f24537 100%);
        background-color: #c62d1f;
    }

    .btn-kumiri:active {
        position: relative;
        top: 1px;
    }
</style>
<body>
<?php echo view('header.php'); ?>
<?php if ($params['error']) : ?>
    <div class="error-heading">
        <span><?= $params['error'] ?></span>
    </div>
<?php endif; ?>
<?php if ($params['message']) : ?>
    <div class="error-heading">
        <span><?= $params['message'] ?></span>
    </div>
<?php endif; ?>
<div class="row">
    <?php if (array_key_exists('players', $params) && !empty($params['players'])) : ?>
        <?php foreach ($params['players'] as $player) : ?>
            <div class="column">
                <img src="/media/players/<?= $player['image']; ?>"
                     style="width:100%" onclick="myFunction(this);">
                <div>
                    <b>Firstname: </b><span><?= $player['firstname'] ?></span>
                </div>
                <div>
                    <b>Lastname: </b><span><?= $player['lastname'] ?></span>
                </div>
                <div>
                    <b>Nickname: </b><span><?= $player['nickname'] ?></span>
                </div>
                <div>
                    <b>Date of birth: </b><span><?= $player['date_of_birth'] ?></span>
                </div>
                <div>
                    <b>Height: </b><span><?= $player['height'] ?></span>
                </div>
                <div>
                    <b>Weight: </b><span><?= $player['weight'] ?></span>
                </div>
                <div>
                    <b>Jersey Number: </b><span><?= $player['jersey_number'] ?></span>
                </div>
                <div>
                    <b>Bio: </b><span><?= $player['small_bio'] ?></span>
                </div>
                <?php if ($_SESSION['user_id']) : ?>
                    <div style="display:flex;">
                        <form method="post" action="/my-favourites?type=favourite">
                            <input type="hidden" name="player-id" value="<?= $player['id'] ?>">
                            <button type="submit" class="btn-favourite">დაამატე რჩეულებში</button>
                        </form>
                        <form method="post" action="/my-favourites?type=kumiri">
                            <input type="hidden" name="player-id" value="<?= $player['id'] ?>">
                            <button type="submit" class="btn-kumiri">დაამატე კუმირად</button>
                        </form>
                    </div>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
</body>