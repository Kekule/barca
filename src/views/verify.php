<!DOCTYPE html>

<head>
    <title>Welcome</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-witdth, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/assets/css/login-style.css">
</head>

<body>
<?php echo view('header.php', ['full' => false]); ?>
<div class="limiter">
    <div class="login-conatiner">
        <?php if ($params['error']) : ?>
            <div class="error-heading">
                <span><?= $params['error'] ?></span>
            </div>
        <?php endif; ?>
        <div class="login-wrap">
            <form class="login-form" method="post" action="/verify">
                <span class="login-title">Verify account</span>

                <div class="validate-input" data-validate="Valid email is required: ex@abc.xyz">
                    <input class="input" type="text" name="code" placeholder="Verification code" id="code">
                    <span class="focus-input"></span>
                    <span class="symbol-input">
                        <i class="fa fa-envelope" aria-hidden="true"></i>
                    </span>
                </div>

                <div class="container-login">
                    <button class="login-btn" type="submit" id="verify">SUBMIT</button>
                </div>
        </div>
        </form>
    </div>
</div>
</div>
</body>
<script src="/assets/js/validateUserVerification.js"></script>