<!DOCTYPE html>

<head>
    <title>Welcome</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-witdth, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/assets/css/login-style.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/index-style.css">
</head>

<body>
<?php echo view('header.php', ['full' => false]); ?>
<div class="limiter">
    <div class="login-conatiner">
        <?php if ($params['error']) : ?>
            <div class="error-heading">
                <span><?= $params['error'] ?></span>
            </div>
        <?php endif; ?>
        <div style="width: 100%;">
            <img class="login_image" src="/assets/images/camp_gif.gif"/></div>
        <div class=" login-wrap ">
            <form class="login-form" method="post" action="/sign-in">
                <span class="login-title ">Login</span>

                <div class="validate-input " data-validate="Valid email is required: ex@abc.xyz ">
                    <input class="input " type="text " name="email" placeholder="Email ">
                    <span class="focus-input "></span>
                    <span class="symbol-input ">
                        <i class="fa fa-envelope " aria-hidden="true "></i>
                    </span>
                </div>

                <div class="validate-input " data-validate="Password is required ">
                    <input class="input " type="password" name="password" placeholder="Password ">
                    <span class="focus-input "></span>
                    <span class="symbol-input ">
                        <i class="fa fa-lock " aria-hidden="true "></i>
                    </span>
                </div>

                <div class="container-login-form-btn ">
                    <button class="login-btn" type="submit" name="submit" id="submit">
                        Login
                    </button>
                </div>

                <div class="text-center p-t-136 ">
                    <a class="txt2 " href="/register">
                        Create your Account
                        <i class="fa fa-long-arrow-right m-l-5 " aria-hidden="true"></i>
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
</body>