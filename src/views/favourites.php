<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/assets/css/index-style.css">
    <meta name="viewport" content="width=device-witdth, initial-scale=1">
    <title>Home</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        var hidden = false;
        $(document).ready(function () {
            $('.act').click(function () {
                if (hidden == false) {
                    $('.article-text').slideToggle(1000);
                    $(this).text("Show");
                    hidden = true;
                } else {
                    $('.article-text').slideToggle(1000);
                    $(this).text("Hide");
                    hidden = false;
                }
            });
        });
    </script>
    <script type="text/javascript">
        function animateImage() {
            console.log("Called");
            $('#messi').css({
                right: '10%'
            });
            $('#messi').animate({
                right: '-100%'
            }, 5000, 'linear', function () {
                animateImage();
            });
        }

        $(document).ready(function () {
            animateImage();
        });
    </script>
    <style>
        .btn-favourite {
            box-shadow: inset 0px 1px 0px 0px #fff6af;
            background: linear-gradient(to bottom, #ffec64 5%, #ffab23 100%);
            background-color: #ffec64;
            border-radius: 6px;
            border: 1px solid #ffaa22;
            display: inline-block;
            cursor: pointer;
            color: #333333;
            font-family: Arial;
            font-size: 15px;
            font-weight: bold;
            padding: 6px 24px;
            text-decoration: none;
            text-shadow: 0px 1px 0px #ffee66;
        }

        .btn-favourite:hover {
            background: linear-gradient(to bottom, #ffab23 5%, #ffec64 100%);
            background-color: #ffab23;
        }

        .btn-favourite:active {
            position: relative;
            top: 1px;
        }

        .btn-kumiri {
            box-shadow: inset 0px 1px 0px 0px #f5978e;
            background: linear-gradient(to bottom, #f24537 5%, #c62d1f 100%);
            background-color: #f24537;
            border-radius: 6px;
            border: 1px solid #d02718;
            display: inline-block;
            cursor: pointer;
            color: #ffffff;
            font-family: Arial;
            font-size: 15px;
            font-weight: bold;
            padding: 6px 24px;
            text-decoration: none;
            text-shadow: 0px 1px 0px #810e05;
        }

        .btn-kumiri:hover {
            background: linear-gradient(to bottom, #c62d1f 5%, #f24537 100%);
            background-color: #c62d1f;
        }

        .btn-kumiri:active {
            position: relative;
            top: 1px;
        }
    </style>
</head>
<body>
<?php echo view('header.php'); ?>
<?php if ($params['error']) : ?>
    <div class="error-heading">
        <span><?= $params['error'] ?></span>
    </div>
<?php endif; ?>
<?php if ($params['message']) : ?>
    <div class="error-heading">
        <span><?= $params['message'] ?></span>
    </div>
<?php endif; ?>
<?php if ($params['kumiri']) : ?>
<div>
    <h1>კუმირი:</h1>
    <div class="kumiri" style="max-width: 500px; padding: 10px">
        <img src="/media/players/<?= $params['kumiri']['image']; ?>"
             style="width:100%" onclick="myFunction(this);">
        <div>
            <b>Firstname: </b><span><?= $params['kumiri']['firstname'] ?></span>
        </div>
        <div>
            <b>Lastname: </b><span><?= $params['kumiri']['lastname'] ?></span>
        </div>
        <div>
            <b>Nickname: </b><span><?= $params['kumiri']['nickname'] ?></span>
        </div>
        <div>
            <b>Date of birth: </b><span><?= $params['kumiri']['date_of_birth'] ?></span>
        </div>
        <div>
            <b>Height: </b><span><?= $params['kumiri']['height'] ?></span>
        </div>
        <div>
            <b>Weight: </b><span><?= $params['kumiri']['weight'] ?></span>
        </div>
        <div>
            <b>Jersey Number: </b><span><?= $params['kumiri']['jersey_number'] ?></span>
        </div>
        <div>
            <b>Bio: </b><span><?= $params['kumiri']['small_bio'] ?></span>
        </div>
        <div style="display:flex;">
            <form method="post" action="/my-favourites?type=delete-kumiri">
                <input type="hidden" name="player-id" value="<?= $params['kumiri']['id'] ?>">
                <button type="submit" class="btn-kumiri">წაშალე კუმირი</button>
            </form>
        </div>
    </div>
</div>
<?php endif; ?>
<?php if (!empty($params['favourites'])) : ?>
    <h2>ფავორიტი მოთამაშეები</h2>
    <?php foreach ($params['favourites'] as $player) : ?>
        <div class="column">
            <img src="/media/players/<?= $player['image']; ?>"
                 style="width:100%" onclick="myFunction(this);">
            <div>
                <b>Firstname: </b><span><?= $player['firstname'] ?></span>
            </div>
            <div>
                <b>Lastname: </b><span><?= $player['lastname'] ?></span>
            </div>
            <div>
                <b>Nickname: </b><span><?= $player['nickname'] ?></span>
            </div>
            <div>
                <b>Date of birth: </b><span><?= $player['date_of_birth'] ?></span>
            </div>
            <div>
                <b>Height: </b><span><?= $player['height'] ?></span>
            </div>
            <div>
                <b>Weight: </b><span><?= $player['weight'] ?></span>
            </div>
            <div>
                <b>Jersey Number: </b><span><?= $player['jersey_number'] ?></span>
            </div>
            <div>
                <b>Bio: </b><span><?= $player['small_bio'] ?></span>
            </div>
            <div style="display:flex;">
                <form method="post" action="/my-favourites?type=delete-favourite">
                    <input type="hidden" name="player-id" value="<?= $player['player_id'] ?>">
                    <button type="submit" class="btn-favourite">წაშალე ფავორიტებიდან</button>
                </form>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
</body>