<link rel="stylesheet" type="text/css" href="/assets/css/buttons.css">
<link rel="stylesheet" type="text/css" href="/assets/admin/css/style.css">
<style>
    table, td, th {
        border: 1px solid black;
    }

    table {
        border-collapse: collapse;
        width: 100%;
    }

    th {
        text-align: left;
    }
</style>

<?= view('admin/common/header.php') ?>
<?php if (!empty($params['users'])) : ?>
<a class="btn-favourite" href="/admin/download-users">Export users</a>
<table>
    <thead>
        <tr>
            <th>id</th>
            <th>username</th>
            <th>email</th>
            <th>is_verified</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($params['users'] as $user) : ?>
        <tr>
            <td><?= $user['id'] ?></td>
            <td><?= $user['username'] ?></td>
            <td><?= $user['email'] ?></td>
            <td><?= $user['is_verified'] ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php else: ?>
<h1>No users so far!</h1>
<?php endif ?>
