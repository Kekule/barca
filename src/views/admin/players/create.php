<html>
<head>
    <title>Add a New Player</title>
    <?= view('admin/common/head.php') ?>
</head>
<body>
<?= view('admin/common/header.php') ?>
<?= view('admin/common/header.php') ?>
<div>
    <?= view('admin/common/messages.php') ?>
    <form method="POST" action="/admin/players/store" enctype="multipart/form-data">
        <div class="field">
            <label class="field-label" for="firstname">Firstname</label>
            <input name="firstname" type="text" id="firstname" placeholder="Firstname..." required>
        </div>
        <div class="field">
            <label class="field-label" for="lastname">Lastname</label>
            <input name="lastname" type="text" id="lastname" placeholder="Lastname..." required>
        </div>
        <div class="field">
            <label class="field-label" for="nickname">Nickname</label>
            <input name="nickname" type="text" id="nickname" placeholder="Nickname...">
        </div>
        <div class="field">
            <label class="field-label" for="date_of_birth">Date Of Birth</label>
            <input name="date_of_birth" type="date" id="date_of_birth" required>
        </div>
        <div class="field">
            <label class="field-label" for="height">Height in cm</label>
            <input name="height" type="number" id="height" placeholder="180" required>
        </div>
        <div class="field">
            <label class="field-label" for="weight">Weight in Kg</label>
            <input name="weight" type="number" id="weight" placeholder="75" required>
        </div>
        <div class="field">
            <label class="field-label" for="small_bio">Small Bio</label>
            <textarea name="small_bio" id="small_bio" placeholder="Small Bio..." required></textarea>
        </div>
        <div class="field">
            <label class="field-label" for="image">Image</label>
            <input name="image" type="file" id="image" accept="image/jpeg,png,gif,jpg">
        </div>
        <div class="field">
            <label class="field-label" for="jersey_number">Jersey Number</label>
            <input name="jersey_number" type="number" id="jersey_number" placeholder="10" required min="0" max="99">
        </div>
        <div class="field">
            <input type="submit" value="Create">
        </div>
    </form>
</div>
</body>
</html>