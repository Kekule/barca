<?php
/** @var \Barca\Models\Player $player */
?>
<html>
<head>
    <title>Add a New Player</title>
    <?= view('admin/common/head.php') ?>
</head>
<body>
<?= view('admin/common/header.php') ?>
<?= view('admin/common/header.php') ?>
<div>
    <?= view('admin/common/messages.php') ?>
    <form method="POST" action="/admin/players/update" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $player->getId() ?>">
        <div class="field">
            <label class="field-label" for="firstname">Firstname</label>
            <input name="firstname" type="text" id="firstname" placeholder="Firstname..." value="<?= htmlspecialchars($player->getFirstname()) ?>" required>
        </div>
        <div class="field">
            <label class="field-label" for="lastname">Lastname</label>
            <input name="lastname" type="text" id="lastname" placeholder="Lastname..." value="<?= htmlspecialchars($player->getLastname()) ?>" required>
        </div>
        <div class="field">
            <label class="field-label" for="nickname">Nickname</label>
            <input name="nickname" type="text" id="nickname" placeholder="Nickname..." value="<?= htmlspecialchars($player->getNickname()) ?>">
        </div>
        <div class="field">
            <label class="field-label" for="date_of_birth">Date Of Birth</label>
            <input name="date_of_birth" type="date" id="date_of_birth" value="<?= htmlspecialchars($player->getDateOfBirth()) ?>" required>
        </div>
        <div class="field">
            <label class="field-label" for="height">Height in cm</label>
            <input name="height" type="number" id="height" placeholder="180" value="<?= htmlspecialchars($player->getHeight()) ?>" required>
        </div>
        <div class="field">
            <label class="field-label" for="weight">Weight in Kg</label>
            <input name="weight" type="number" id="weight" placeholder="75" value="<?= htmlspecialchars($player->getWeight()) ?>" required>
        </div>
        <div class="field">
            <label class="field-label" for="small_bio">Small Bio</label>
            <textarea name="small_bio" id="small_bio" placeholder="Small Bio..." required><?= htmlspecialchars($player->getSmallBio()) ?></textarea>
        </div>
        <div class="field">
            <label class="field-label" for="image">Image</label>
            <input name="image" type="file" id="image" accept="image/jpeg,png,gif,jpg">
            <label class="field-label">Old Image:</label>
            <img src="/media/players/<?= $player->getImage() ?>" width="100px">
        </div>
        <div class="field">
            <label class="field-label" for="jersey_number">Jersey Number</label>
            <input name="jersey_number" type="number" id="jersey_number" placeholder="10" required min="0" max="99" value="<?= $player->getJerseyNumber() ?>">
        </div>
        <div class="field">
            <input type="submit" value="Save">
        </div>
    </form>
</div>
</body>
</html>
