<?php
/** @var Barca\Models\Player[] $players */

?>
<html>
<head>
    <title>Players</title>
    <?= view('admin/common/head.php') ?>
</head>
<body>
<?= view('admin/common/header.php') ?>
<?= view('admin/common/messages.php') ?>
<div>
    <a href="/admin/players/create">Add a New Player</a>
    <button id="mass-delete">Delete Selected</button>
    <form method="POST" action="/admin/players/mass-delete" style="display: none" id="mass-delete-form">
        <input id="items" name="items" value="">
    </form>
</div>
<table>
    <thead>
    <tr>
        <th></th>
        <th>ID</th>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Nickname</th>
        <th>Date of Birth</th>
        <th>Height</th>
        <th>Weight</th>
        <th>Small Bio</th>
        <th>Image</th>
        <th>Jersey Number</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($players as $player): ?>
    <tr>
        <td><input type="checkbox" class="tick" data-id="<?= $player->getId() ?>"></td>
        <td><?= $player->getId() ?></td>
        <td><?= $player->getFirstname() ?></td>
        <td><?= $player->getLastname() ?></td>
        <td><?= $player->getNickname() ?></td>
        <td><?= $player->getDateOfBirth() ?></td>
        <td><?= $player->getHeight() ?></td>
        <td><?= $player->getWeight() ?></td>
        <td><?= $player->getSmallBio() ?></td>
        <td><img src="<?= '/media/players/' . $player->getImage() ?>" width="50px" height="50px"></td>
        <td><?= $player->getJerseyNumber() ?></td>
        <td><a href="/admin/players/edit?id=<?= $player->getId() ?>">Edit</a></td>
    </tr>
    <?php endforeach ?>
    </tbody>
</table>
<script src="/assets/admin/scripts/grid.js"></script>
</body>
</html>
