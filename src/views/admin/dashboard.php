<?php
/** @var \Barca\Models\AdminUser $admin */
?>
<html>
<head>
    <title>Dashboard</title>
    <?= view('admin/common/head.php') ?>
</head>
<body>
<?= view('admin/common/header.php') ?>
<h1>Welcome <?= $admin->getFirstname() ?> <?= $admin->getLastname() ?>!</h1>
</body>
</html>
