<html>
<head>
    <title>Log In</title>
    <?= view('admin/common/head.php') ?>
</head>
<body>
<div class="login-form">
    <form action="/admin/login-post" method="POST">
        <?php if (isset($errors)): ?>
            <?= view('admin/common/errors-list.php', ['errors' => $errors]) ?>
        <?php endif ?>
        <div class="field">
            <label class="field-label" for="username">Username</label>
            <input type="text" id="username" name="username" placeholder="username..." required>
        </div>
        <div class="field">
            <label class="field-label" for="password">Password</label>
            <input type="password" id="password" name="password" placeholder="password..." required>
        </div>
        <div class="field">
            <input type="submit" value="Log In">
        </div>
    </form>
</div>
</body>
</html>
