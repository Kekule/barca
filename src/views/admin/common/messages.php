<?php if (isset($_SESSION['messages']) && count($_SESSION['messages']) > 0): ?>
    <ul class="errors-list-success">
        <?php foreach ($_SESSION['messages'] as $message): ?>
            <li class="error-success"><?= htmlentities($message) ?></li>
        <?php endforeach ?>
    </ul>
<?php unset($_SESSION['messages']) ?>
<?php endif ?>

<?php if (isset($_SESSION['errors']) && count($_SESSION['errors']) > 0): ?>
    <ul class="errors-list">
        <?php foreach ($_SESSION['errors'] as $message): ?>
            <li class="error"><?= htmlentities($message) ?></li>
        <?php endforeach ?>
    </ul>
    <?php unset($_SESSION['errors']) ?>
<?php endif ?>

