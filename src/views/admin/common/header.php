<header>
    <ul>
        <li>
            <a class="btn-favourite" href="/admin/users">Users</a>
        </li>
        <li>
            <a class="btn-favourite" href="/admin/players">Players</a>
        </li>
        <li>
            <a class="btn-kumiri" href="javascript:document.getElementById('logout-form').submit()">Logout</a>
            <form method="POST" action="/admin/logout" style="display: none" id="logout-form">
                <input name="logout" value="">
            </form>
        </li>
    </ul>
</header>