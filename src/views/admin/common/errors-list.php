<?php if (isset($errors) && count($errors) > 0): ?>
<ul class="errors-list">
    <?php foreach ($errors as $error): ?>
    <li class="error"><?= htmlentities($error) ?></li>
    <?php endforeach ?>
</ul>
<?php endif ?>
