<?php
/** @var array $pagerData */
?>
<ul>
    <?php if (count($pagerData['prevPages']) > 0): ?>
    <li><a href="/<?= \Barca\Application\Request::getPathname() ?>?page=<?= $pagerData['prevPages'][0] ?>"</li>
    <?php endif ?>
</ul>