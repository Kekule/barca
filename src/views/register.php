<!DOCTYPE html>

<head>
    <title>Welcome</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-witdth, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/assets/css/login-style.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/index-style.css">
</head>

<body>
<?php echo view('header.php', ['full' => false]); ?>
<div class="limiter">
    <div class="login-conatiner">
        <?php if ($params['error']) : ?>
            <div class="error-heading">
                <span><?= $params['error'] ?></span>
            </div>
        <?php endif; ?>
        <div class="login-wrap">
            <form class="login-form" method="post" action="/register">
                <span class="login-title">Register</span>

                <div class="validate-input" data-validate="Valid email is required: ex@abc.xyz">
                    <input class="input" type="text" name="email" placeholder="Email" id="email">
                    <span class="focus-input"></span>
                    <span class="symbol-input">
                        <i class="fa fa-envelope" aria-hidden="true"></i>
                    </span>
                </div>

                <div class="validate-input" data-validate="Valid username required">
                    <input class="input" type="text" name="username" placeholder="Username" id="username">
                    <span class="focus-input"></span>
                    <span class="symbol-input">
                        <i class="fa fa-envelope" aria-hidden="true"></i>
                    </span>
                </div>

                <div class="validate-input" data-validate="Password is required">
                    <input class="input" type="password" name="password" placeholder="Password" id="password">
                    <span class="focus-input"></span>
                    <span class="symbol-input">
                        <i class="fa fa-lock" aria-hidden="true"></i>
                    </span>
                </div>

                <div class="validate-input" data-validate="Password is required">
                    <input class="input" type="password" name="pass" placeholder="Confirm Password" id="confirm-password">
                    <span class="focus-input"></span>
                    <span class="symbol-input">
                        <i class="fa fa-lock" aria-hidden="true"></i>
                    </span>
                </div>

                <div class="container-login">
                    <button class="login-btn" type="submit" id="submit">
                        Register
                    </button>
                </div>
        </div>
        </form>
    </div>
</div>
</div>
</body>
<script src="/assets/js/validateUserRegistration.js"></script>