<?php
/**
 * აბრუნებს ფაილის მისამართს public დირექტორიაში
 * @param string $path
 * @return string
 */
function public_path($path = '')
{
    return dirname(__DIR__) . '/public/' . $path;
}

/**
 * აბრუნებს config.php ფაილიდან კონფიგურაციას (ფაილი იტვირთება ერთხელ)
 * @return array
 */
function config() {
    static $config = null;
    if (!$config) {
        $config = require dirname(__DIR__) . '/config.php';
    }
    return $config;
}

/**
 * აბრუნებს ფაილის მისამართს views ფოლდერში
 * @param $template
 * @return string
 */
function templatePath($template) {
    return __DIR__ . '/views/' . $template;
}
/**
 * არენდერებს view-ს, შესაძლებელია პარამეტრების გადაცემა key value pairების სახით
 * ვიუში value-ს მიწვდებით $key ცვლდაით
 * @param string $template
 * @param array $params
 * @return false|string
 */
function view($template, $params = []) {
    foreach ($params as $name => $value) {
        $$name = $value;
    }
    ob_start();
    require templatePath($template);
    return ob_get_clean();
}