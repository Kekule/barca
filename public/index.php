<?php
/**
 * აპლიკაციის საწყისი წერტილი
 * /media/* /assets/* url-ების გარდა ყველა url გაივლის ამ ფაილს
 */

/**
 * აიმპორტებს აუცილებელ კლასებს და ფუნქციებს
 */
try {
    require_once '../vendor/autoload.php';
    require_once '../src/functions.php';
} catch (Exception $e) {
    die('AUTOLOAD ERROR');
}

session_start();

use Barca\Application\Application;

/**
 * ეშვება აპლიკაცია
 */
(new Application())->run();
