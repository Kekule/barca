window.addEventListener('load', () => {
    const verification = document.getElementById('verify');

    verification.addEventListener('click', (e) => {
        const code = document.getElementById('code');

        if (!validateVerificationCode(code)) {
            e.preventDefault();
            const codeError = document.getElementById('codeError') || document.createElement('span');
            codeError.setAttribute('class', 'error');
            codeError.setAttribute('id', 'codeError');
            codeError.textContent = 'Verification code must be 6 digit and should contain only integers';
            code.parentNode.appendChild(codeError);
        } else {
            if (document.getElementById('codeError')) document.getElementById('codeError').remove();
        }
    })
})

validateVerificationCode = (code) => {
    const reg = /^\d+$/;
    return reg.test(code.value) && code.value.length === 6;
}
