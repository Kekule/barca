window.addEventListener('load', () => {
    const submit = document.getElementById('submit');

    submit.addEventListener('click', (e) => {
        const username = document.getElementById('username');
        const email = document.getElementById('email');
        const password = document.getElementById('password');
        const confirmation = document.getElementById('confirm-password');

        if (!validateUserName(username)) {
            e.preventDefault();
            const userError = document.getElementById('userError') || document.createElement('span');
            userError.setAttribute('class', 'error');
            userError.setAttribute('id', 'userError');
            userError.textContent = 'Username must be at least 3 characters longs';
            username.parentNode.appendChild(userError);
        } else {
            if (document.getElementById('userError')) document.getElementById('userError').remove();
        }

        if (!validateEmail(email)) {
            e.preventDefault();
            const emailError = document.getElementById('emailError') || document.createElement('span');
            emailError.setAttribute('class', 'error');
            emailError.setAttribute('id', 'emailError');
            emailError.textContent = 'Email is not valid';
            email.parentNode.appendChild(emailError);
        } else {
            if (document.getElementById('emailError')) document.getElementById('emailError').remove();
        }

        if (!validatePassword(password)) {
            e.preventDefault();
            const passwordError = document.getElementById('passwordError') || document.createElement('span');
            passwordError.setAttribute('class', 'error');
            passwordError.setAttribute('id', 'passwordError');
            passwordError.textContent = 'Password must be at least 8 characters, must container lower and upper case letters and numbers';
            password.parentNode.appendChild(passwordError);
        } else {
            if (document.getElementById('passwordError')) document.getElementById('passwordError').remove();
        }

        if (!validatePasswordMatch(password, confirmation)) {
            e.preventDefault();
            const matchError = document.getElementById('matchError') || document.createElement('span');
            matchError.setAttribute('class', 'error');
            matchError.setAttribute('id', 'matchError');
            matchError.textContent = 'Passwords do not match';
            confirmation.parentNode.appendChild(matchError);
        } else {
            if (document.getElementById('matchError')) document.getElementById('matchError').remove();
        }
    })
})

validateUserName = (username) => {
    return username && username.value.length > 3;
}

validateEmail = (email) => {
    const reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    return reg.test(email.value);
}

validatePassword = (password) => {
    const reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;
    return reg.test(password.value);
}

validatePasswordMatch = (password, confirmation) => {
    return password.value === confirmation.value;
}
