window.addEventListener('load', function () {
    const massDeleteButton = document.querySelector('#mass-delete');
    massDeleteButton.addEventListener('click', function () {
        const selectedItems = [];

        document.querySelectorAll('.tick:checked').forEach(function (item) {
            selectedItems.push(item.dataset.id);
        });

        document.querySelector('#items').value = JSON.stringify(selectedItems);
        document.querySelector('#mass-delete-form').submit();
    });
});
