# BARCA

# გაშვების ინსტრუქცია

1. შექმენით ბაზა
`create database barca;`
2. ჩააიმპორტეთ ბაზის დამფი
`source <PATH-TO-DUMP.SQL>`
3. განაახლეთ config.php ფაილი სწორი მონაცემებით: host, dbName, user, password
4. განაახლეთ სმტპ პარამეტრები config.php ფაილში: host, username, password
5. გაუშვით სერვერი `public` დირექტორიიდან `cd public && php -S localhost:8000` 
6. გახსენით ლინკი ბრაუზერში: [http://localhost:8000](http://localhost:8000)
7. ადმინისტრატორის პანელისთვის გახსენით: [http://localhost:8000/admin](http://localhost:8000/admin)
8. ადმინის იუზერი: `admin`
9. ადმინის პაროლი: `option123`
